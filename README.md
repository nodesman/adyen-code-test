# NotionWorks Office Recommendation

Select the best NotionWorks' office to go to.  

 

## Assumptions

* The city of origin can be anywhere in the world. 
* The first 5 days after arrival are the only days that matter for weather. The data that is available for weather 5 days [after are too unpredictable to be of any use.](https://www.minitab.com/en-us/Published-Articles/Weather-Forecasts--Just-How-Reliable-Are-They-/)
* The employee is going to be traveling economy class.
* The traveler is at the departure city in question or nearby.
* There are no military airports in the world.
* Good weather is defined as:
 - Temperature in the range defined by the employee
 - Preference for sunny days over cloudy / overcast days. 
 - Precipitation in any degree is detrimental in how often it will be, and to how much.
 - No one enjoys weather less than 10. No one enjoys greater than 25. 

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
