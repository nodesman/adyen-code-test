const LOCATION_STATE = {
  LOCATION_PENDING: 0,
  LOCATION_AVAILABLE: 1,
  LOCATION_DENIED: 2,
  LOCATION_FAILED: 3
}

let tomorrow = new Date()
tomorrow.setHours(0)
tomorrow.setMinutes(0)
tomorrow.setTime(tomorrow.getTime() + 86400000)
const initialState = {
  location_status: LOCATION_STATE.LOCATION_PENDING,
  location: null,
  pending_airport_list: true,
  fetching_airports_list_failed: false,
  airports: null,
  temperatureRange: [10, 25],
  departureAirports: [],
  overallRank: [],
  airlineScores: {
    amsterdam: 0,
    budapest: 0,
    madrid: 0
  },
  weatherScores: {
    amsterdam: 0,
    budapest: 0,
    madrid: 0
  },
  weatherData: {
    amsterdam: null,
    madrid: null,
    budapest: null
  },
  departure_date: tomorrow,
  trips: {
    amsterdam: [],
    madrid: [],
    budapest: []
  },
  selected: {
    amsterdam: null,
    budapest: null,
    madrid: null
  }
}

export {initialState, LOCATION_STATE}
