import {initialState, LOCATION_STATE} from './state'
import Requests from './requests'
import Vue from 'vue'

class OfficeRecommender {
  constructor () {
    this.data = initialState
    window.addEventListener('DOMContentLoaded', this.init.bind(this))
  }

  getLocationSuccess (location) {
    this.data.location_status = LOCATION_STATE.LOCATION_AVAILABLE
    this.data.location = location
    this.fetchAirports()
  }

  getLocationFailed (error) {
    switch (error.code) {
      // eslint-disable-next-line
      case (GeolocationPositionError.PERMISSION_DENIED):
        this.data.location_status = LOCATION_STATE.LOCATION_DENIED
        break
      default:
        this.data.location_status = LOCATION_STATE.LOCATION_FAILED
    }
  }

  async _getWeather () {
    let data = await Requests.fetchWeatherData()
    Vue.set(this.data.weatherData, 'amsterdam', data.amsterdam)
    Vue.set(this.data.weatherData, 'budapest', data.budapest)
    Vue.set(this.data.weatherData, 'madrid', data.madrid)
    this._calculateWeatherScores()
  }

  _farenheightToCelcius (f) {
    return (f - 32) * 5 / 9
  }

  _cityScore (day) {
    let cloudCoverScores = day.DailyForecasts.map(day => {
      return (day.Icon < 6) ? 2 : -1
    })

    let cloudCoverFinalScore = cloudCoverScores.reduce((acc, current) => {
      return acc + current
    }, 0)

    let cloudCoverFinalRating = ((cloudCoverFinalScore + 5) / 15) * 10

    let preferedRange = this.data.temperatureRange
    let preferedRangeSize = preferedRange[1] - preferedRange[0]
    let temperatureOverlapScore = day.DailyForecasts.map(day => {
      let currentOverlap = preferedRangeSize

      let max = this._farenheightToCelcius(day.Temperature.Maximum.Value)
      let min = this._farenheightToCelcius(day.Temperature.Minimum.Value)

      if (min > preferedRange[0]) {
        currentOverlap -= min - preferedRange[0]
      }
      if (max < preferedRange[1]) {
        currentOverlap -= preferedRange[1] - max
      }
      return currentOverlap
    })

    let temperatureScore = temperatureOverlapScore.reduce((acc, overlap) => {
      return acc + overlap
    }, 0)

    let temperatureFinalScore = (temperatureScore / (preferedRangeSize * 5)) * 10

    return cloudCoverFinalRating + temperatureFinalScore
  }

  _calculateWeatherScores () {
    this.data.weatherScores.amsterdam = this._cityScore(this.data.weatherData.amsterdam)
    this.data.weatherScores.budapest = this._cityScore(this.data.weatherData.budapest)
    this.data.weatherScores.madrid = this._cityScore(this.data.weatherData.madrid)
    this._calculateOverallRank()
  }

  setDate (newDate) {
    this.data.departure_date = newDate
  }

  async refreshItinerary () {
    let trips = await Requests.fetchItineraries(this.data.departureAirports, this.data.departure_date)
    Vue.set(this.data.trips, 'amsterdam', trips.amsterdam)
    Vue.set(this.data.trips, 'budapest', trips.budapest)
    Vue.set(this.data.trips, 'madrid', trips.madrid)

    let comparisonFunction = (left, right) => {
      if (left.price < right.price) {
        return -1
      } else if (left.price === right.price) {
        return 0
      } else {
        return 1
      }
    }

    this.data.trips.amsterdam.sort(comparisonFunction)
    this.data.trips.madrid.sort(comparisonFunction)
    this.data.trips.budapest.sort(comparisonFunction)

    this.data.selected.amsterdam = this.data.trips.amsterdam[0]
    this.data.selected.budapest = this.data.trips.budapest[0]
    this.data.selected.madrid = this.data.trips.madrid[0]

    this._calculateAirlineScores()
  }

  async fetchAirports () {
    try {
      this.data.airports = await Requests.fetchAirports(this.data.location)
      this.data.airports = this.data.airports.filter(airport => {
        return ['AMS', 'BUD', 'MAD'].indexOf(airport.code) === -1
      })

      this.data.pending_airport_list = false
    } catch (err) {
      this.data.pending_airport_list = false
      this.data.fetching_airports_list_failed = true
    }
  }

  _calculateAirlineScores () {
    if (!this.data.selected.amsterdam || !this.data.selected.budapest.price || !this.data.selected.madrid.price) {
      console.log('here!')
      Vue.set(this.data.airlineScores, 'amsterdam', 0)
      Vue.set(this.data.airlineScores, 'madrid', 0)
      Vue.set(this.data.airlineScores, 'budapest', 0)
      return
    }
    let prices = [this.data.selected.amsterdam.price, this.data.selected.budapest.price, this.data.selected.madrid.price]
    let maximum = Math.max.apply(Math, prices)

    Vue.set(this.data.airlineScores, 'amsterdam', 10 - (this.data.selected.amsterdam.price / maximum) * 10)
    Vue.set(this.data.airlineScores, 'madrid', 10 - (this.data.selected.madrid.price / maximum) * 10)
    Vue.set(this.data.airlineScores, 'budapest', 10 - (this.data.selected.budapest.price / maximum) * 10)
    this._calculateOverallRank()
  }

  _calculateOverallRank () {
    let amsterdam = {
      name: 'Amsterdam',
      weather: this.data.weatherScores.amsterdam,
      airline: this.data.airlineScores.amsterdam,
      score: this.data.airlineScores.amsterdam + this.data.weatherScores.amsterdam
    }

    let budapest = {
      name: 'Budapest',
      weather: this.data.weatherScores.budapest,
      airline: this.data.airlineScores.budapest,
      score: this.data.airlineScores.budapest + this.data.weatherScores.budapest
    }

    let madrid = {
      name: 'Madrid',
      weather: this.data.weatherScores.madrid,
      airline: this.data.airlineScores.madrid,
      score: this.data.airlineScores.madrid + this.data.weatherScores.madrid
    }

    let list = [amsterdam, budapest, madrid]

    list.sort((left, right) => {
      if (left.score === right.score) {
        return 0
      }
      return left.score < right.score ? 1 : -1
    })

    Vue.set(this.data, 'overallRank', list)
  }

  setSelected (city, item) {
    Vue.set(this.data.selected, city, item)
    this._calculateAirlineScores()
  }

  setDepartureAirports (newValue) {
    this.data.departureAirports = newValue
    this._calculateAirlineScores()
  }

  init () {
    navigator.geolocation.getCurrentPosition(this.getLocationSuccess.bind(this), this.getLocationFailed.bind(this))
    this._getWeather()
  }

  setTemperatureRange (value) {

    Vue.set(this.data, 'temperatureRange', value)
    this._calculateWeatherScores()

  }
}

let
  instance = new OfficeRecommender()

export default instance
