var Requests = {
  async fetchAirports (location) {
    let latitude = location.coords.latitude
    let longitude = location.coords.longitude
    let response = await fetch(`https://cometari-airportsfinder-v1.p.rapidapi.com/api/airports/by-radius?radius=200&lng=${longitude}&lat=${latitude}`, {
      'method': 'GET',
      'headers': {
        'x-rapidapi-host': 'cometari-airportsfinder-v1.p.rapidapi.com',
        'x-rapidapi-key': 'TivPoY2R1HmshOuLydkFyKqzuT9Jp13LkkejsnPZ2ZfBdwFz1Z'
      }
    })
    return response.json()
  },

  _trimTripData (data) {

    return data.data.map(trip => {
      return {
        from: trip.flyFrom,
        to: trip.flyTo,
        price: trip.price,
        route: trip.route.map(leg => {
          return {
            from: leg.flyFrom,
            to: leg.flyTo,
            airline: leg.airline
          }
        }),
        duration: trip.fly_duration,
        departure_time: trip.dTime * 1000
      }
    })
  },

  async fetchWeatherData () {

    let amsterdam = await fetch('http://dataservice.accuweather.com/forecasts/v1/daily/5day/249758?apikey=wbrEpy2TgC4ypqnKlCNV5oypnGXlrQFn')
    let madrid = await fetch('http://dataservice.accuweather.com/forecasts/v1/daily/5day/308526?apikey=wbrEpy2TgC4ypqnKlCNV5oypnGXlrQFn')
    let budapest = await fetch('http://dataservice.accuweather.com/forecasts/v1/daily/5day/187423?apikey=wbrEpy2TgC4ypqnKlCNV5oypnGXlrQFn')

    amsterdam = await amsterdam.json()
    madrid = await madrid.json()
    budapest = await budapest.json()

    return {
      amsterdam,
      madrid,
      budapest
    }

  },

  async fetchItineraries (departureAirports, departureDateObj) {

    let departureDate = departureDateObj.getDate()
    departureDate = departureDate < 10 ? '0' + departureDate : departureDate

    let departureMonth = departureDateObj.getMonth() + 1
    departureMonth = departureMonth < 10 ? '0' + departureMonth : departureMonth

    let departureYear = departureDateObj.getFullYear()

    let dateString = `${departureDate}/${departureMonth}/${departureYear}`

    let fetchAmsterdam = `https://api.skypicker.com/flights?partner=nodesmanadyencodetest&curr=EUR&fly_from=${departureAirports}&fly_to=AMS&date_from=${dateString}&date_to=${dateString}`
    let fetchBudapest = `https://api.skypicker.com/flights?partner=nodesmanadyencodetest&curr=EUR&fly_from=${departureAirports}&fly_to=BUD&date_from=${dateString}&date_to=${dateString}`
    let fetchMadrid = `https://api.skypicker.com/flights?partner=nodesmanadyencodetest&curr=EUR&fly_from=${departureAirports}&fly_to=MAD&date_from=${dateString}&date_to=${dateString}`

    let requests = []
    let requestOptions = {
      method: 'GET',
      cache: 'no-cache'
    }
    requests[0] = await fetch(fetchAmsterdam, requestOptions)
    requests[1] = await fetch(fetchBudapest, requestOptions)
    requests[2] = await fetch(fetchMadrid, requestOptions)

    let promises = requests.map(request => {
      return request.json()
    })

    let data = await Promise.all(promises)

    return {
      amsterdam: this._trimTripData(data[0]),
      budapest: this._trimTripData(data[1]),
      madrid: this._trimTripData(data[2])
    }
  }
}

export default Requests
