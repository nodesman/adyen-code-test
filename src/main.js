// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import data from './core'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import 'element-ui/lib/theme-chalk/index.css'

Vue.config.productionTip = false

locale.use(lang)

Vue.use(ElementUI)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  data,
  template: '<App :data="data"/>'
})
